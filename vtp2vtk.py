#!/usr/bin/env python
"""File format conversion
Convert .vtp to .vtk and binary vtk to ASCII vtk"""
import sys
import vtk

def vtp_to_vtk(invtkfile, outvtpfile, binary=False):
    reader = vtk.vtkXMLPolyDataReader()
    reader.SetFileName(invtkfile)
    writer = vtk.vtkPolyDataWriter()
    writer.SetFileName(outvtpfile)
    if binary:
        writer.SetFileTypeToBinary()
    writer.SetInputConnection(reader.GetOutputPort())
    writer.Update()
    print(outvtpfile)
    
def vtk_to_vtk(invtkfile, outvtpfile, binary=False):
    reader = vtk.vtkUnstructuredGridReader()
    reader.SetFileName(invtkfile)
    writer = vtk.vtkUnstructuredGridWriter()
    writer.SetFileName(outvtpfile)
    if binary:
        writer.SetFileTypeToBinary()
    writer.SetInputConnection(reader.GetOutputPort())
    writer.Update()
    print(outvtpfile)

if __name__ == '__main__':
    args = sys.argv
    binary = False
    if '-b' in args:
        args.remove('-b')
        binary = True
    if len(args) < 2:
        print('Batch converts vtk files to vtp files.\nUsage:\n    vtk2vtp.py model1.vtk model2.vtk ...')
        print('    [-b] causes output to be in binary format, much smaller file size, if it happens to work')
        # sys.exit()
        infiles = [
            'Surfaces and Centerlines/surfCenterlines_NUH026R.vtp',
            'Surfaces and Centerlines/surfClip_NUH026R.vtp',
            'Surfaces and Centerlines/surfCenterlines_NUH041L.vtp',
            'Surfaces and Centerlines/surfClip_NUH041L.vtp',
            'Surfaces and Centerlines/surfCenterlines_NUH064L.vtp',
            'Surfaces and Centerlines/surfClip_NUH064L.vtp',
            'Surfaces and Centerlines/surfCenterlines_NUH088R.vtp',
            'Surfaces and Centerlines/surfClip_NUH088R.vtp',
            'NUH026R_399.vtk'
            ]
    else:
        infiles = args[1:]

    for vtkfile in infiles:
        if vtkfile[-4:] == '.vtp':
            vtp_to_vtk(vtkfile, vtkfile[:-4]+'.vtk', binary=binary)
        elif vtkfile[-4:] == '.vtk':
            vtk_to_vtk(vtkfile, vtkfile[:-4]+'_.vtk', binary=binary)
        else:
            print(vtkfile, "doesn't look like a vtp file, won't convert")
            continue
