# See Mode

1 September 2018 at 16:27

As promised, here is the data analysis task: here you find 4 vtk files resulting from CFD simulations in carotid arteries. You also have access to surfaces and centrelines for these meshes. Write a piece of code that:
Automatically (or with the least user input) finds and visualizes the narrowest section of the artery (the stenotic throat).
Returns a volumetric average of pressure in the stenotic throat.
Creates a cross section at the stenotic throat, and returns the average wall shear stress on that cross section. (It would be a circle (sort of!) and you need to calculate and return a line average of the wall shear stress around this circle.)

I think a timeframe of 2-3 days should be a good time to have some time to work on it. Let me know if this works for you. Thanks again for your time and hope you have some fun with the tech task!
