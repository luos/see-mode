#!/usr/bin/env python
import sys
import numpy as np
import vtk
import vtkInterface

filelists = [
    ['Surfaces and Centerlines/surfCenterlines_NUH026R.vtp',
    'Surfaces and Centerlines/surfClip_NUH026R.vtp',
    'CFD Results/VTK_NUH026R/NUH026R_399.vtk',
    'CFD Results/VTK_NUH026R/default-interior/default-interior_399.vtk'],
    ['Surfaces and Centerlines/surfCenterlines_NUH041L.vtp',
    'Surfaces and Centerlines/surfClip_NUH041L.vtp',
    'CFD Results/VTK_NUH041L/NUH041L_300.vtk',
    'CFD Results/VTK_NUH041L/default-interior/default-interior_300.vtk'],
    ['Surfaces and Centerlines/surfCenterlines_NUH064L.vtp',
    'Surfaces and Centerlines/surfClip_NUH064L.vtp',
    'CFD Results/VTK_NUH064L/NUH064L_406.vtk',
    'CFD Results/VTK_NUH064L/default-interior/default-interior_406.vtk'],
    ['Surfaces and Centerlines/surfCenterlines_NUH088R.vtp',
    'Surfaces and Centerlines/surfClip_NUH088R.vtp',
    'CFD Results/VTK_NUH088R/NUH088R_353.vtk',
    'CFD Results/VTK_NUH088R/default-interior/default-interior_353.vtk']
]

# Set which dataset to load
data_index = 0
if len(sys.argv) >= 2:
    data_index = int(sys.argv[1])

[centerline_file, surface_file, cfd_file, cfd_interior_file] = filelists[data_index]

def readXMLPolyData(filename):
   reader = vtk.vtkXMLPolyDataReader()
   reader.SetFileName(filename)
   reader.Update()
   return vtkInterface.PolyData(reader.GetOutput())

def normalize_L1(normal):
    norm = np.linalg.norm(normal, ord=1)
    if norm != 0:
        normal /= norm
    return normal

# Read the centerline from file
centerline = readXMLPolyData(centerline_file)
MaximumInscribedSphereRadius = centerline.GetPointScalars('MaximumInscribedSphereRadius')
val, idx = min((val, idx) for (idx, val) in enumerate(MaximumInscribedSphereRadius))
center = centerline.points[idx]
print('The stenotic throat is at position', center, 'on the centerline with MaximumInscribedSphereRadius =', val)

# Check if there are sufficient points on the centerline
if len(centerline.points) < 2:
    print('Error: there are only', len(centerline.points), 'points on the centerline.')

# Calculate normal for a clipping plane along the centerline
normal = (center - centerline.points[idx-1]) if idx > 0 else (centerline.points[1] - centerline.points[0])
normal = normalize_L1(normal)
# disk = vtkInterface.Cylinder(center, normal, val*2, 0)

# Read the surface from file
surface = readXMLPolyData(surface_file)
ClippingArray = surface.GetPointScalars('ClippingArray')

# Create a clipping plane to cut in the centerline direction
clipping_plane = vtk.vtkPlane()
clipping_plane.SetOrigin(center[0], center[1], center[2])
clipping_plane.SetNormal(normal[0], normal[1], normal[2])

# Create a cutter for centerline
cutter1 = vtk.vtkCutter()
cutter1.SetCutFunction(clipping_plane)
cutter1.SetInputData(centerline.Copy())
cutter1.Update()
centerline_clip = vtkInterface.PolyData(cutter1.GetOutput())

# Create a cutter for surface
cutter2 = vtk.vtkCutter()
cutter2.SetCutFunction(clipping_plane)
cutter2.SetInputData(surface.Copy())
cutter2.Update()
surface_clip = vtkInterface.PolyData(cutter2.GetOutput())

# Initialize the position and direction for a clipping plane between two artery branches
midpoint = [0, 0, 0] 
direction = [0, 1, 0]
cross_section = surface_clip
if len(centerline_clip.points) >= 2:
    midpoint = (centerline_clip.points[0] + centerline_clip.points[1]) / 2
    direction = normalize_L1(centerline_clip.points[0] - centerline_clip.points[1])
    cross_section = surface_clip.ClipPlane(midpoint, direction, inplace=False)

# Read CFD results from files
cfd = vtkInterface.UnstructuredGrid(cfd_file)
cfd_interior = vtkInterface.PolyData(cfd_interior_file)

# Rescale CFD results to match the magnitude of the surface and the centerline
cfd.points *= 1000
cfd_interior.points *= 1000

# Create a cutter for CFD
cutter3 = vtk.vtkCutter()
cutter3.SetCutFunction(clipping_plane)
cutter3.SetInputData(cfd.Copy())
cutter3.Update()
cfd_clip = vtkInterface.PolyData(cutter3.GetOutput())
if len(centerline_clip.points) >= 2:
    cfd_clip = cfd_clip.ClipPlane(midpoint, direction, inplace=False)

# Create a cutter for CFD interior
cutter4 = vtk.vtkCutter()
cutter4.SetCutFunction(clipping_plane)
cutter4.SetInputData(cfd_interior.Copy())
cutter4.Update()
cfd_interior_clip = vtkInterface.PolyData(cutter4.GetOutput())
if len(centerline_clip.points) >= 2:
    cfd_interior_clip = cfd_interior_clip.ClipPlane(midpoint, direction, inplace=False)

# Get wallShearStress on the edges and calculate average wall shear stress (magnitudes) on the cross section
cfd_clip_edges = cfd_clip.ExtractEdges()
wallShearStress = cfd_clip_edges.GetPointScalars('wallShearStress')
if wallShearStress is not None:
    print('The average wall shear stress on the cross section is', np.mean(np.linalg.norm(wallShearStress, axis=1)))

# Calculate volumetric average of pressure
p = cfd_clip.GetPointScalars('p')
if p is not None:
    print('The volumetric average of pressure is', np.mean(p))

# Calculate volumetric average of phi
phi = cfd_interior_clip.GetCellScalars('phi')
if phi is not None:
    print('The volumetric average of phi is', np.mean(phi))

# Create plotting object and add meshes to plot
plobj = vtkInterface.PlotClass()
# plobj.AddMesh(disk)
plobj.AddMesh(surface, scalars=ClippingArray)
plobj.AddMesh(centerline, scalars=MaximumInscribedSphereRadius)
plobj.AddMesh(cross_section, 'm', linethick=3)
plobj.AddMesh(cfd_clip_edges, 'b', linethick=3)
plobj.AddMesh(cfd_interior_clip)
plobj.AddText(cfd_file.split('/')[1])
plobj.Plot()
