import vtkInterface
from vtkInterface import examples

# load and shrink airplane
airplane = vtkInterface.PolyData(examples.planefile)
pts = airplane.GetNumpyPoints() # gets pointer to array
pts /= 10 # shrink by 10x

# rotate and translate ant so it is on the plane
ant = vtkInterface.PolyData(examples.antfile)
ant.RotateX(90)
ant.Translate([90, 60, 15])

# Make a copy and add another ant
ant_copy = ant.Copy()
ant_copy.Translate([30, 0, -10])

# Create plotting object
plobj = vtkInterface.PlotClass()
plobj.AddMesh(ant, 'r')
plobj.AddMesh(ant_copy, 'b')

# Add airplane mesh and make the color equal to the Y position.  Add a
# scalar bar associated with this mesh
plane_scalars = pts[:, 1]
plobj.AddMesh(airplane, scalars=plane_scalars, stitle='Airplane Y\nLocation')

# Add annotation text
plobj.AddText('Ants and Plane Example')
plobj.Plot()